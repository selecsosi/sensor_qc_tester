# -*- coding: utf-8 -*-
import math

class SensorClassifier(object):

    def __init__(self, sensor, reference):
        self.sensor = sensor
        self.reference = reference

    def classify(self):
        raise NotImplementedError("ABC does not implement validation, please use concrete subclass")


class TemperatureSensorClassifier(SensorClassifier):

    def classify(self):
        # Sum the readings
        reading_total = sum([x[0] for x in self.sensor.readings])
        # Calculate the median
        reading_median = reading_total / len(self.sensor.readings)
        # Sum the total deviation from the mean
        deviation_squared_total = sum([(x[0] - reading_median) ** 2 for x in self.sensor.readings])
        # Calculate the standard deviation from the median
        sensor_std = math.sqrt(deviation_squared_total / len(self.sensor.readings))
        #Test for ultraprecise
        if sensor_std < 3 and abs(reading_median - self.reference) <= 0.5:
            self.sensor.classification = "ultra precise"
            self.sensor.css_cls = "success"
        elif sensor_std < 5 and abs(reading_median - self.reference) <= 0.5:
            self.sensor.classification = "very precise"
            self.sensor.css_cls = "info"
        else:
            self.sensor.classification = "precise"
            self.sensor.css_cls = "active"




class HumiditySensorClassifier(SensorClassifier):

    def classify(self):
        for reading in self.sensor.readings:
            # Test for exceeding the max variation
            if abs(reading[0] - self.reference) > (0.01 * self.reference):
                self.sensor.classification = "discard"
                self.sensor.css_cls = "danger"
                return # Short circuit
        self.sensor.classification = "OK"
        self.sensor.css_cls = "success"