# -*- coding: utf-8 -*-
from classifiers import TemperatureSensorClassifier, HumiditySensorClassifier, SensorClassifier


class Sensor(object):

    def __init__(self, name):
        self.name = name
        self.readings = []
        self.classification = None
        self.classifier = None

    def set_classifier(self, reference):
        if hasattr(self, "classifier_cls"):
            self.classifier = self.classifier_cls(self, reference)
        else:
            pass

class TemperatureSensor(Sensor):

    classifier_cls = TemperatureSensorClassifier

class HumiditySensor(Sensor):

    classifier_cls = HumiditySensorClassifier