#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from string import Template
import sys
import argparse

from collections import OrderedDict
from datetime import datetime

from sensors import TemperatureSensor, HumiditySensor

REFERENCE_KEY = "reference"
THERMOSTAT_LABEL = "thermometer"
HUMIDITY_LABEL = "humidity"
DATE_FORMAT = '%Y-%m-%dT%H:%M'

class UnknownSensorException(BaseException):
    pass

class ReferenceException(BaseException):
    pass

class References(object):
    def __init__(self, temp_reference, humidity_reference):
        self.temp_reference = temp_reference
        self.humidity_reference = humidity_reference

def parse_input_from_stdin():
    reference = None
    sensor_dict = OrderedDict()
    for line in sys.stdin:
        # Clean up input
        strip_line = line.strip()
        # Test that we still have content left
        if len(strip_line) > 0:
            # Split the line into its entries
            entry = strip_line.split()
            # Process the entry
            result = parse_entry_list_from_stdin(entry, sensor_dict)
            # If entry was a reference, store that reference
            if isinstance(result, References):
                reference = result
            elif reference is not None:
                if isinstance(result, TemperatureSensor):
                    result.set_classifier(reference.temp_reference)
                elif isinstance(result, HumiditySensor):
                    result.set_classifier(reference.humidity_reference)
            else:
                raise ReferenceException("No reference data set")

    return reference, sensor_dict


def parse_entry_list_from_stdin(entry, sensor_dict):
    """
    Returns object corresponding to entry line or parses data of
    already registered object
    """
    key_entry = entry[0]
    result = None
    if key_entry == REFERENCE_KEY:
        result = parse_reference(entry)
    elif key_entry == THERMOSTAT_LABEL:
        if not sensor_dict.has_key(entry[1]):
            result = TemperatureSensor(entry[1])
            sensor_dict[entry[1]] = result
    elif key_entry == HUMIDITY_LABEL:
        if not sensor_dict.has_key(entry[1]):
             result = HumiditySensor(entry[1])
             sensor_dict[entry[1]] = result
    else:
        parse_reading_entry(entry, sensor_dict)
    return result

def parse_reference(entry):
    """
    Takes reference entry and constructs References object
    """
    if len(entry) == 3:
        return References(float(entry[1]), float(entry[2]))
    else:
        raise ReferenceException("Missing reference data, expected format reference <temp_ref> <humidity_ref>\nGot %s" % entry)



def parse_reading_entry(entry, sensor_dict):
    """
    Takes entry and updates the sensor dict with a reading
    """
    name = entry[1]
    if sensor_dict.has_key(name):
        sensor = sensor_dict[name]
        entry_value = float(entry[2])
        try:
            entry_date = datetime.strptime(entry[0], DATE_FORMAT)
            sensor.readings.append([entry_value, entry_date])
        except ValueError as e:
            # Date parsing failed
            #Could log or do other things
            raise e
    else:
        #error
        raise UnknownSensorException("Data listed for unknown sensor type %s" % name)

def classify_sensors(sensor_dict):
    for key, sensor in sensor_dict.items():
        sensor.classifier.classify()

def template_resulte_with_jinja(sensor_dict, reference, output_filepath, env):
    template = env.get_template('results.html')
    html = template.render(
        title="Results count: %s" % len(sensor_dict),
        results=[v for k,v in sensor_dict.items()],
        reference=reference
    )

    path = os.path.abspath(output_filepath)
    with open(path, "wb") as f:
        f.write(html)

def template_result_from_file(sensor_dict, ouput_filepath):
    #Template if user doesn't have jinja, not as nice
    template = None
    with open("templates/results_no_jinja.html") as f:
        template_txt = f.read()
        template = Template(template_txt)
    title = "Results count: %s" % len(sensor_dict)
    row_html = '<tr class="{0}"><td>{1}</td><td>{2}</td><td>{3}</td></tr>'
    tbody = ""
    counter = 0
    for k, sensor in sensor_dict.iteritems():
        counter += 1
        tbody += row_html.format(sensor.css_cls, counter, sensor.name, sensor.classification)
    html = template.substitute(tbody=tbody, title=title)
    path = os.path.abspath(ouput_filepath)
    with open(path, "wb") as f:
        f.write(html)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output",
                        type=str,
                        help="output result in html to specified file")
    args = parser.parse_args()
    env = None
    output_html = None
    #If we are outputting to html, load jinja2
    if args.output:
        output_html = args.output
        try:
            from jinja2 import Environment, PackageLoader
            env = Environment(loader=PackageLoader('sensor_qc_tester', 'templates'))
        except ImportError as e:
            pass

    # parse the input from stdin, capturing the reference object for classification
    reference, sensor_dict = parse_input_from_stdin()
    classify_sensors(sensor_dict)
    # if env is set, we can output to a template
    if env is not None:
        template_resulte_with_jinja(sensor_dict, reference, output_html, env)
    elif output_html:
        template_result_from_file(sensor_dict, output_html)
    else:
        # Ouput to stdout using print
        print "Test Results\n"
        for key, sensor in sensor_dict.items():
            print "%s: %s" % (sensor.name, sensor.classification)

if __name__ == "__main__":
    sys.exit(main())